#!/bin/bash

# set envs
export $(cat /opt/cron.env | grep -v '==' | xargs )

DATE_FROM=$(date -d '10 years ago' +'%s000000')
DATE_TO=$(date -d "`echo $RETENTION_DAYS` days ago" +'%s000000')

# delete
curl -X DELETE "$ES_HOST/$ES_INDEX/_query?pretty=1&q=machine_name:$ES_MACHINE_NAME" -d "{"query":{"range":{"timestamp":{"from":$DATE_FROM,"to": $DATE_TO}}}}"

# clean es cache
curl -X GET "$ES_HOST/$ES_INDEX/_flush"

