#!/bin/bash

set -e

CRON_SCHEDULE=${CRON_SCHEDULE:=0 0 * * *}
BACKEND=${BACKEND:="elasticsearch"}
RETENTION_DAYS=${RETENTION_DAYS:='7'}
ES_INDEX=${ES_INDEX:='cadvisor'}

LOGFIFO='/var/log/cron.fifo'
if [[ ! -e "$LOGFIFO" ]]; then
    mkfifo "$LOGFIFO"
fi

# save env
env > /opt/cron.env

echo -e "$CRON_SCHEDULE /opt/backends/$BACKEND.sh > $LOGFIFO 2>&1" | crontab -
crontab -l
cron
tail -f "$LOGFIFO"
