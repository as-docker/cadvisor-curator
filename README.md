This image allows to clean cadvisor data older then ''x'' days by running a cron script. Currently it supports only elasticsearch backend. You can find link to repository [here](https://bitbucket.org/as-docker/cadvisor-curator).

### Usage

    docker run -it --env-file=curator.env --name=cadvisor-curator --rm amsdard/cadvisor-curator

### Parameters:

* `RETENTION_DAYS=7`: retention days
* `BACKEND=elasticsearch`: cadvisor backend, one of: elasticsearch.

### Elasticsearch params ###
* `ES_HOST`: elasticsearch host with protocol
* `ES_INDEX=cadvisor`: cadvisor index name
* `ES_MACHINE_NAME`: machine name value, to filter documents

