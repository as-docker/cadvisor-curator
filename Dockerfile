FROM tutum/curl
MAINTAINER Tomasz Rzany <tomasz.rzany@amsterdam-standard.pl>

ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ADD backends /opt/backends
RUN chmod +x /opt/backends/*.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD [""]
